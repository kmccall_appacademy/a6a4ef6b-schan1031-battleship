class Board
  attr_reader :grid

  def initialize(grid = nil)
    @grid = grid || Board.default_grid
  end

  def self.default_grid
    Array.new(10) { Array.new(10) }
  end

  def count
    # ct = 0
    # @grid.each_index do |idx|
    #   ct += @grid[idx].count(:s)
    # end
    # ct
    @grid.inject(0) { |accum, row| accum += row.count(:s) }
  end

  def empty?(pos = [])
    if pos.empty?
      return false if count > 0
      return true if count < 1
    else
      return true if @grid[pos[0]][pos[1]].nil?
      return false if !@grid[pos[0]][pos[1]].nil?
    end
  end

  def full?
    return true if count == @grid.size ** 2
    false
  end

  def place_random_ship
    raise 'Full board' if full?
    locx = rand(0..1)
    locy = rand(0..1)
    if @grid[locx][locy] == nil
      @grid[locx][locy] = :s
    end
  end

  def won?
    return true if empty?
    return false unless empty?
  end

end
