class BattleshipGame
  attr_reader :board, :player

  def initialize(board, player)
    @board = board
    @player = player
  end

  def attack(pos)
    @board.grid[pos[0]][pos[1]] = :x
  end

end
